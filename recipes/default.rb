db_master_node_name = node['lamp-cluster-config']['db']['master']

case
when node.roles.include?('lamp-cluster-app')
  include_recipe "lamp-cluster::app"
when node.roles.include?('lamp-cluster-db')
  if node.name == db_master_node_name
    include_recipe "lamp-cluster::db"
  else
    include_recipe "lamp-cluster::db_slave"
  end
else
  Chef::Log.warn "node #{node} has neither role"
end
