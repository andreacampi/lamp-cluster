#this recipe knows how to set up a database for the whole cluster

Chef::Log.info "Configuring node as a MySQL master"

node.set['mysql']['bind_address'] = node.ipaddress
node.set['mysql']['tunable']['server_id'] = 1
node.set['mysql']['tunable']['log_bin'] = 'master-bin'

include_recipe "lamp-node-db"
