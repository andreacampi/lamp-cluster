Chef::Log.info "Configuring node as a MySQL slave"

cluster_id = node['lamp-cluster-config']['cluster_id']
db_master = node['lamp-cluster-config']['db']['master']

node.set['mysql']['tunable']['server_id'] = 2
node.set['mysql']['tunable']['log_bin'] = nil

include_recipe "lamp-node-db"
include_recipe "mysql::ruby"

require 'mysql'

ruby_block "start MySQL slave replication" do
  block do
    dbmasters = search(:node, "name:#{db_master} AND lamp-cluster-config_cluster_id:#{cluster_id}")

    if dbmasters.size != 1
      Chef::Log.error("#{dbmasters.size} database masters, cannot set up replication!")
    else
      dbmaster = dbmasters.first
      Chef::Log.info("Using #{dbmaster.name} as master")

      m = Mysql.new("localhost", "root", node['mysql']['server_root_password'])
      command = %Q{
      CHANGE MASTER TO
        MASTER_HOST="#{dbmaster['mysql']['bind_address']}",
        MASTER_USER="repl",
        MASTER_PASSWORD="#{dbmaster['mysql']['server_repl_password']}";
      }
      Chef::Log.info "Sending start replication command to mysql: "
      Chef::Log.info "#{command}"

      m.query("stop slave")
      m.query(command)
      m.query("start slave")
    end
  end
  not_if do
    #TODO this fails if mysql is not running - check first
    m = Mysql.new("localhost", "root", node['mysql']['server_root_password'])
    slave_sql_running = ""
    m.query("show slave status") {|r| r.each_hash {|h| slave_sql_running = h['Slave_SQL_Running'] } }
    slave_sql_running == "Yes"
  end
end
