#this recipe knows how to connect an application server to the whole cluster

include_recipe "lamp-node-app"

ip_address = cluster_db_ipaddress

template "/tmp/app.conf" do
  source "app.conf.erb"
  variables :db_master_ip => ip_address
end
