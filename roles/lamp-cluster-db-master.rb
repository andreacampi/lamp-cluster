#
# Generic role for every LAMP cluster; do not put anything specific to a single cluster in here.
#

name "lamp-cluster-db-master"
description "System is added to the LAMP cluster as a MySQL db server (master)"
