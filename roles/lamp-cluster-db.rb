#
# Generic role for every LAMP cluster; do not put anything specific to a single cluster in here.
#

name "lamp-cluster-db"
description "System is added to the LAMP cluster as a MySQL db server"
run_list  "recipe[lamp-cluster]"
