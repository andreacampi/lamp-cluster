#
# Generic role for every LAMP cluster; do not put anything specific to a single cluster in here.
#

name "lamp-cluster-app"
description "System is added to the LAMP cluster as an unicorn rails app server"
run_list  "recipe[lamp-cluster]"
